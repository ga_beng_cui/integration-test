#!/usr/bin/bash

# Copyright (c) 2020 Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.

# #############################################
# @Author    :   liujingjing
# @Contact   :   liujingjing25812@163.com
# @Date      :   2020/10/19
# @License   :   Mulan PSL v2
# @Desc      :   The usage of commands in multipath-tools package
# ############################################

source "common_multipath-tools.sh"
function pre_test() {
    LOG_INFO "Start to prepare the test environment."
    deploy_env
    check_free_disk
    local_disk=${disk_list[-1]}
    LOG_INFO "End to prepare the test environment."
}

function run_test() {
    LOG_INFO "Start to run test."
    multipath -t | grep -E "devices|blacklist_exceptions|blacklist|defaults|overrides"
    CHECK_RESULT $?
    multipath -r -v3 >rdebug 2>&1
    CHECK_RESULT $?
    grep "delegating" rdebug
    CHECK_RESULT $?
    multipath -i -v3 /dev/mapper/mpatha >idebug 2>&1
    CHECK_RESULT $?
    grep "scope limited to 3600" idebug
    CHECK_RESULT $?
    cd /etc/multipath/ || exit 1
    multipath -B bindings -v3 >/tmp/Bdebug 2>&1
    CHECK_RESULT $?
    grep "binding" /tmp/Bdebug
    CHECK_RESULT $?
    multipath -b bindings -v3 /dev/mapper/mpatha >/tmp/bdebug 2>&1
    CHECK_RESULT $?
    grep "loaded successfully" /tmp/bdebug
    CHECK_RESULT $?
    cd - || exit 1
    multipath -v3 -p multibus >pdebug 2>&1
    CHECK_RESULT $?
    grep "multipath" pdebug
    CHECK_RESULT $?
    multipath -c /dev/${local_disk} | grep "MULTIPATH"
    CHECK_RESULT $?
    multipath -W /dev/dm-2 | grep "successfully reset wwids"
    CHECK_RESULT $?
    multipath -w /dev/dm-2 | grep "removed"
    CHECK_RESULT $?
    LOG_INFO "End to run test."
}

function post_test() {
    LOG_INFO "Start to restore the test environment."
    clear_env
    LOG_INFO "End to restore the test environment."
}

main "$@"
