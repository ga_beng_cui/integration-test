#!/usr/bin/bash

# Copyright (c) 2020 Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.

# #############################################
# @Author    :   liujingjing
# @Contact   :   liujingjing25812@163.com
# @Date      :   2020/10/19
# @License   :   Mulan PSL v2
# @Desc      :   The usage of commands in multipath-tools package
# ############################################

source "common_multipath-tools.sh"
function pre_test() {
    LOG_INFO "Start to prepare the test environment."
    deploy_env
    check_free_disk
    local_disk=${disk_list[-1]}
    LOG_INFO "End to prepare the test environment."
}

function run_test() {
    LOG_INFO "Start to run test."
    multipath -v3 | grep ${local_disk}
    CHECK_RESULT $?
    multipath -ll | grep "mpath" -A 10
    CHECK_RESULT $?
    multipath -l | grep "mpath" -A 10
    CHECK_RESULT $?
    multipath -v3 -f /dev/dm-2
    CHECK_RESULT $?
    test -L /dev/mapper/mpatha1
    CHECK_RESULT $? 1
    service multipathd restart
    multipath -v3 -R 1 -F
    CHECK_RESULT $?
    test -L /dev/mapper/mpatha
    CHECK_RESULT $? 1
    service multipathd restart
    multipath -a /dev/dm-2 | grep "added"
    CHECK_RESULT $?
    grep "0000" /etc/multipath/wwids
    CHECK_RESULT $?
    multipath -v3 -C /dev/dm-2 >check_log 2>&1
    CHECK_RESULT $?
    grep -E "checker|sda|/dev/dm-2" check_log
    CHECK_RESULT $?
    multipath -v3 -q >qdebug 2>&1
    CHECK_RESULT $?
    grep -C 10 "paths list" qdebug
    CHECK_RESULT $?
    multipath -v3 -d >ddebug 2>&1
    CHECK_RESULT $?
    grep -i "dev" ddebug
    CHECK_RESULT $?
    LOG_INFO "End to run test."
}

function post_test() {
    LOG_INFO "Start to restore the test environment."
    clear_env
    LOG_INFO "End to restore the test environment."
}

main "$@"
