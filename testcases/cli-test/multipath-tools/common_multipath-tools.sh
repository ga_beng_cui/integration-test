#!/usr/bin/bash
# Copyright (c) 2020 Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.
####################################
# @Author    :   liujingjing
# @Contact   :   liujingjing25812@163.com
# @Date      :   2020/10/19
# @License   :   Mulan PSL v2
# @Desc      :   Public class integration
#####################################

source "$OET_PATH/libs/locallibs/common_lib.sh"

function deploy_env() {
    TEST_DISK1=/dev/$(SSH_CMD "lsblk | grep disk | tail -n 1 | awk '{print \$1}'" ${NODE2_IPV4} ${NODE2_PASSWORD} ${NODE2_USER} | tail -n 1 | sed 's/\r//')
    SSH_CMD "dnf install -y scsi-target-utils; 
        echo -e 'n\np\n1\n\n+2000M\nw' | fdisk ${TEST_DISK1}; 
        echo -e '<target iqn.2013-12.com.make:ws.httpd>\nbacking-store ${TEST_DISK1}\n</target>' >>/etc/tgt/targets.conf; 
        systemctl restart tgtd; 
        systemctl stop firewalld;
        " ${NODE2_IPV4} ${NODE2_PASSWORD} ${NODE2_USER}
    DNF_INSTALL "iscsi-initiator-utils multipath-tools device-mapper-event device-mapper"
    systemctl restart iscsid
    iscsiadm -m discovery -t sendtargets -p ${NODE2_IPV4}
    iscsiadm -m node -T iqn.2013-12.com.make:ws.httpd -l
    mpathconf --enable --with_multipathd y
    service multipathd start
    multipath -v2
    multipath -ll
    echo "
defaults {
       user_friendly_names       yes
       max_fds                   max
       queue_without_daemon      no
       flush_on_last_del         yes
}

devices {
       device {
               vendor                  \"IET \"
               product                 \"VIRTUAL-DISK\"
               path_grouping_policy    multibus
               getuid_callout          \"/sbin/scsi_id -g -u -s/block/%n\"
               path_checker            directio
               path_selector           \"round-robin 0\"
               hardware_handler        \"0\"
               failback                15
               rr_weight               priorities
               no_path_retry           queue
               rr_min_io               100
               product_blacklist       LUNZ
       }
}" >/etc/multipath.conf
    lsmod | grep dm_multipath || modprobe dm_multipath
    modprobe dm_multipath
    service multipathd restart
    chkconfig --level 345 multipathd on
}

function clear_env() {
    SSH_CMD "dnf remove -y scsi-target-utils; 
        echo -e 'd\nw\n' | fdisk ${TEST_DISK1}" ${NODE2_IPV4} ${NODE2_PASSWORD} ${NODE2_USER}
    iscsiadm -m node --logoutall=all
    multipath -F
    DNF_REMOVE "multipath-tools iscsi-initiator-utils device-mapper-event"
    del_file=$(ls | grep -vE ".sh")
    rm -rf ${del_file} /tmp/disk1
}

function check_free_disk() {
    disk_list=($(lsblk | awk '{print$1" "$6}' | grep disk | grep sd | awk '{print$1}'))
    for disk in ${disk_list[@]}; do
        lsblk | awk '{print$1" "$6" "$7}' | grep / | awk '{print$1" "$2}' | grep ${disk}
        if [ $? -eq 0 ]; then
            disk_list=(${disk_list[@]/${disk}})
            continue
        fi
    done
}
