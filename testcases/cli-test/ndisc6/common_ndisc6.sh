#!/usr/bin/bash
# Copyright (c) 2020 Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.
####################################
# @Author    :   liujingjing
# @Contact   :   liujingjing25812@163.com
# @Date      :   2020/10/12
# @License   :   Mulan PSL v2
# @Desc      :   Public class integration
#####################################

source "$OET_PATH/libs/locallibs/common_lib.sh"
function deploy_env() {
    share_arg
    DNF_INSTALL "ndisc6 xinetd time"
    hostname newlocalhost
    cp /etc/resolv.conf /etc/resolv.conf-bak
    sed -i 's/name/#&/' /etc/resolv.conf
    sed -i '6s/yes/no/g' /etc/xinetd.d/echo-stream
    systemctl restart xinetd
    SSH_CMD "dnf install -y xinetd;
        sed -i '6s/yes/no/g' /etc/xinetd.d/echo-stream;
        systemctl restart xinetd;
        " ${NODE2_IPV4} ${NODE2_PASSWORD} ${NODE2_USER}
}

function clear_env() {
    share_arg
    SSH_CMD "dnf remove -y xinetd " ${NODE2_IPV4} ${NODE2_PASSWORD} ${NODE2_USER}
    DNF_REMOVE "ndisc6 xinetd time"
    cp /etc/resolv.conf-bak /etc/resolv.conf
    rm -rf file runtime /etc/resolv.conf-bak
}
