#!/usr/bin/bash

# Copyright (c) 2020 Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.

# #############################################
# @Author    :   liujingjing
# @Contact   :   liujingjing25812@163.com
# @Date      :   2020/11/10
# @License   :   Mulan PSL v2
# @Desc      :   The usage of commands in pcp-import-ganglia2pcp binary package
# ############################################

source "$OET_PATH/libs/locallibs/common_lib.sh"

function pre_test() {
    LOG_INFO "Start to prepare the test environment."
    DNF_INSTALL "pcp-import-ganglia2pcp httpd"
    cp /etc/yum.repos.d/openEuler.repo /etc/yum.repos.d/openEuler.repo-bak
    if [ "${NODE1_FRAME}" == "x86_64" ]; then
    echo -e "\n[fedora]\nname=fedora\nbaseurl=https://archives.fedoraproject.org/pub/archive/fedora/linux/releases/29/Everything/x86_64/os/\nenabled=1\ngpgcheck=0 " >>/etc/yum.repos.d/openEuler.repo
    else
    echo -e "\n[fedora]\nname=fedora\nbaseurl=https://archives.fedoraproject.org/pub/archive/fedora/linux/releases/29/Everything/aarch64/os/\nenabled=1\ngpgcheck=0 " >>/etc/yum.repos.d/openEuler.repo
    fi
    DNF_INSTALL "ganglia ganglia-gmetad ganglia-gmond ganglia-web rrdtool"
    setenforce 0
    systemctl stop firewalld
    sed -i "s/data_source \"my cluster\" localhost/data_source \"cluster01\" ${NODE1_IPV4}/g" /etc/ganglia/gmetad.conf
    service gmetad restart 
    service gmond restart
    service httpd restart
    set timeout 120
    SSH_CMD "cp /etc/yum.repos.d/openEuler.repo /etc/yum.repos.d/openEuler.repo-bak;
        if \[ '$NODE1_FRAME' == 'x86_64' \]; then
            echo -e '\n\[fedora\]\nname=fedora\nbaseurl=https://archives.fedoraproject.org/pub/archive/fedora/linux/releases/29/Everything/x86_64/os/\nenabled=1\ngpgcheck=0 ' >>/etc/yum.repos.d/openEuler.repo
        else
            echo -e '\n\[fedora\]\nname=fedora\nbaseurl=https://archives.fedoraproject.org/pub/archive/fedora/linux/releases/29/Everything/aarch64/os/\nenabled=1\ngpgcheck=0 ' >>/etc/yum.repos.d/openEuler.repo
        fi;
        dnf install -y ganglia-gmond; 
        systemctl stop firewalld;
        setenforce 0;
        sed -i '/ name / s/unspecified/cluster01/' /etc/ganglia/gmond.conf
        service gmond restart;" ${NODE2_IPV4} ${NODE2_PASSWORD} ${NODE2_USER}
        SLEEP_WAIT 120
    LOG_INFO "End to prepare the test environment."
}

function run_test() {
    LOG_INFO "Start to run test."
    ganglia2pcp -f gangpcp -d ./ -Z UTC -h localhost /var/lib/ganglia/rrds/unspecified/${NODE2_IPV4}/
    CHECK_RESULT $?
    grep -aE "localhost|UTC" gangpcp.index
    CHECK_RESULT $?
    test -f gangpcp.0 -a -f gangpcp.meta && rm -rf gangpcp.0 gangpcp.meta gangpcp.index
    CHECK_RESULT $?
    LOG_INFO "End to run test."
}

function post_test() {
    LOG_INFO "Start to restore the test environment."
    DNF_REMOVE "pcp-import-ganglia2pcp httpd ganglia ganglia-gmetad ganglia-gmond ganglia-web"
    rm -rf /etc/yum.repos.d/openEuler.repo
    cp /etc/yum.repos.d/openEuler.repo-bak /etc/yum.repos.d/openEuler.repo
    rm -rf /etc/yum.repos.d/openEuler.repo-bak
    SSH_CMD "dnf remove -y ganglia-gmond;
        rm -rf /etc/yum.repos.d/openEuler.repo
        cp /etc/yum.repos.d/openEuler.repo-bak /etc/yum.repos.d/openEuler.repo;
        rm -rf /etc/yum.repos.d/openEuler.repo-bak;
        " ${NODE2_IPV4} ${NODE2_PASSWORD} ${NODE2_USER}
    LOG_INFO "End to restore the test environment."
}

main "$@"
